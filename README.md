# shiro-redis-spring-boot-web-starter

#### 介绍
基于shiro-redis的spring-boot-web-starter


#### 使用说明

引入Maven依赖即可：

```
<dependency>
    <groupId>org.iherus.shiro</groupId>
    <artifactId>shiro-redis-spring-boot-web-starter</artifactId>
    <version>2.5.0</version>
</dependency>
```

