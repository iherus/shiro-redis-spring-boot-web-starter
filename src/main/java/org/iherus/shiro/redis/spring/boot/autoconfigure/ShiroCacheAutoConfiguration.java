/**
 * Copyright (c) 2016-2021, Bosco.Liao (bosco_liao@126.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.iherus.shiro.redis.spring.boot.autoconfigure;

import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.config.web.autoconfigure.ShiroWebAutoConfiguration;
import org.iherus.shiro.cache.redis.RedisCacheManager;
import org.iherus.shiro.cache.redis.RedisSessionDAO;
import org.iherus.shiro.cache.redis.connection.RedisConnectionFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Optional;

/**
 * ShiroCacheAutoConfiguration
 *
 * @author Bosco.Liao
 * @since 2.0.0
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnProperty(name = "shiro.redis.cache.enabled", matchIfMissing = true)
@AutoConfigureBefore(ShiroWebAutoConfiguration.class)
@EnableConfigurationProperties(ShiroRedisProperties.class)
@Import({JedisConnectionFactoryConfiguration.class, LettuceConnectionFactoryConfiguration.class,
        RedissonConnectionFactoryConfiguration.class, CompatibleRedisConnectionFactoryConfiguration.class})
public class ShiroCacheAutoConfiguration {

    private final ShiroRedisProperties properties;

    public ShiroCacheAutoConfiguration(ShiroRedisProperties properties) {
        this.properties = properties;
    }

    @Bean("shiroCacheManager")
    @ConditionalOnMissingBean
    public CacheManager cacheManager(RedisConnectionFactory connectionFactory) {
        RedisCacheManager cacheManager = new RedisCacheManager();
        cacheManager.setConnectionFactory(connectionFactory);
        cacheManager.setKeyPrefix(this.properties.getCache().getKeyPrefix());
        cacheManager.setExpiration(this.properties.getCache().getExpiration());
        cacheManager.setScanBatchSize(this.properties.getCache().getBatchOptions().getScanBatchSize());
        cacheManager.setFetchBatchSize(this.properties.getCache().getBatchOptions().getFetchBatchSize());
        cacheManager.setDeleteBatchSize(this.properties.getCache().getBatchOptions().getDeleteBatchSize());

        Optional.ofNullable(this.properties.getCache().getDatabase()).ifPresent(cacheManager::setDatabase);
        return cacheManager;
    }

    @Bean("redisSessionDAO")
    @ConditionalOnMissingBean
    public SessionDAO sessionDAO(CacheManager cacheManager) {
        RedisSessionDAO sessionDAO = new RedisSessionDAO(cacheManager);
        return sessionDAO;
    }

}
