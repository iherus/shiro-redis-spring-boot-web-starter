/**
 * Copyright (c) 2016-2021, Bosco.Liao (bosco_liao@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.iherus.shiro.redis.spring.boot.autoconfigure;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocketFactory;

/**
 * 
 * @author Bosco.Liao
 * @since 2.0.0
 */
class JedisSslClientConfiguration {

	private final SSLSocketFactory sslSocketFactory;
	private final SSLParameters sslParameters;
	private final HostnameVerifier hostnameVerifier;

	JedisSslClientConfiguration(Builder builder) {
		this.sslSocketFactory = builder.sslSocketFactory;
		this.sslParameters = builder.sslParameters;
		this.hostnameVerifier = builder.hostnameVerifier;
	}

	public SSLSocketFactory getSslSocketFactory() {
		return sslSocketFactory;
	}

	public SSLParameters getSslParameters() {
		return sslParameters;
	}

	public HostnameVerifier getHostnameVerifier() {
		return hostnameVerifier;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private SSLSocketFactory sslSocketFactory;
		private SSLParameters sslParameters;
		private HostnameVerifier hostnameVerifier;

		private Builder() {

		}

		public Builder sslSocketFactory(SSLSocketFactory sslSocketFactory) {
			this.sslSocketFactory = sslSocketFactory;
			return this;
		}

		public Builder sslParameters(SSLParameters sslParameters) {
			this.sslParameters = sslParameters;
			return this;
		}

		public Builder hostnameVerifier(HostnameVerifier hostnameVerifier) {
			this.hostnameVerifier = hostnameVerifier;
			return this;
		}

		public JedisSslClientConfiguration build() {
			return new JedisSslClientConfiguration(this);
		}

	}

}
