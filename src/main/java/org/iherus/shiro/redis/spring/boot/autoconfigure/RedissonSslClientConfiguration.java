/**
 * Copyright (c) 2016-2021, Bosco.Liao (bosco_liao@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.iherus.shiro.redis.spring.boot.autoconfigure;

import java.net.URL;

/**
 * 
 * @author Bosco.Liao
 * @since 2.0.0
 */
class RedissonSslClientConfiguration {

	private final URL sslTruststore;
	private final String sslTruststorePassword;
	private final URL sslKeystore;
	private final String sslKeystorePassword;

	RedissonSslClientConfiguration(Builder builder) {
		this.sslTruststore = builder.sslTruststore;
		this.sslTruststorePassword = builder.sslTruststorePassword;
		this.sslKeystore = builder.sslKeystore;
		this.sslKeystorePassword = builder.sslKeystorePassword;
	}

	public URL getSslTruststore() {
		return sslTruststore;
	}

	public String getSslTruststorePassword() {
		return sslTruststorePassword;
	}

	public URL getSslKeystore() {
		return sslKeystore;
	}

	public String getSslKeystorePassword() {
		return sslKeystorePassword;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private URL sslTruststore;
		private String sslTruststorePassword;
		private URL sslKeystore;
		private String sslKeystorePassword;

		private Builder() {

		}

		public Builder sslTruststore(URL sslTruststore) {
			this.sslTruststore = sslTruststore;
			return this;
		}

		public Builder sslTruststorePassword(String sslTruststorePassword) {
			this.sslTruststorePassword = sslTruststorePassword;
			return this;
		}

		public Builder sslKeystore(URL sslKeystore) {
			this.sslKeystore = sslKeystore;
			return this;
		}

		public Builder sslKeystorePassword(String sslKeystorePassword) {
			this.sslKeystorePassword = sslKeystorePassword;
			return this;
		}

		public RedissonSslClientConfiguration build() {
			return new RedissonSslClientConfiguration(this);
		}

	}

}
