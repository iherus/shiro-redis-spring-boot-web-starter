/**
 * Copyright (c) 2016-2021, Bosco.Liao (bosco_liao@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.iherus.shiro.redis.spring.boot.autoconfigure;

import java.time.Duration;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.RedisURI;
import io.lettuce.core.TimeoutOptions;
import io.lettuce.core.resource.ClientResources;

/**
 * 
 * @author Bosco.Liao
 * @since 2.0.0
 */
class LettuceClientConfiguration {

	private final ClientResources clientResources;
	private final ClientOptions clientOptions;
	private final Duration timeout;

	LettuceClientConfiguration(Builder builder) {
		this.clientResources = builder.clientResources;
		this.clientOptions = builder.clientOptions;
		this.timeout = builder.timeout;
	}

	public ClientResources getClientResources() {
		return clientResources;
	}

	public ClientOptions getClientOptions() {
		return clientOptions;
	}

	public Duration getTimeout() {
		return timeout;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private ClientResources clientResources;
		private ClientOptions clientOptions;
		private Duration timeout;

		private Builder() {
			this.clientResources = ClientResources.create();
			this.clientOptions = ClientOptions.builder().timeoutOptions(TimeoutOptions.enabled()).build();
			this.timeout = Duration.ofSeconds(RedisURI.DEFAULT_TIMEOUT);
		}

		public Builder timeout(Duration timeout) {
			this.timeout = timeout;
			return this;
		}

		public Builder clientResources(ClientResources clientResources) {
			this.clientResources = clientResources;
			return this;
		}

		public Builder clientOptions(ClientOptions clientOptions) {
			this.clientOptions = clientOptions;
			return this;
		}

		public LettuceClientConfiguration build() {
			return new LettuceClientConfiguration(this);
		}

	}

}
