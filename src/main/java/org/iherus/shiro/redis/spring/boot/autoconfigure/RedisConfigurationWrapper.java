/**
 * Copyright (c) 2016-2021, Bosco.Liao (bosco_liao@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.iherus.shiro.redis.spring.boot.autoconfigure;

import org.iherus.shiro.cache.redis.config.RedisClusterConfiguration;
import org.iherus.shiro.cache.redis.config.RedisSentinelConfiguration;
import org.iherus.shiro.cache.redis.config.RedisStandaloneConfiguration;
import org.iherus.shiro.redis.spring.boot.autoconfigure.ShiroRedisProperties.ClusterConfig;
import org.iherus.shiro.redis.spring.boot.autoconfigure.ShiroRedisProperties.SentinelConfig;
import org.springframework.beans.factory.ObjectProvider;

/**
 * RedisConfigurationWrapper
 *
 * @author Bosco.Liao
 * @since 2.0.0
 */
abstract class RedisConfigurationWrapper {

	private final ShiroRedisProperties properties;

	private final RedisStandaloneConfiguration standaloneConfiguration;

	private final RedisSentinelConfiguration sentinelConfiguration;

	private final RedisClusterConfiguration clusterConfiguration;

	protected RedisConfigurationWrapper(ShiroRedisProperties properties,
                                        ObjectProvider<RedisStandaloneConfiguration> standaloneConfigProvider,
                                        ObjectProvider<RedisSentinelConfiguration> sentinelConfigProvider,
                                        ObjectProvider<RedisClusterConfiguration> clusterConfigProvider) {
		this.properties = properties;
		this.standaloneConfiguration = standaloneConfigProvider.getIfUnique();
		this.sentinelConfiguration = sentinelConfigProvider.getIfUnique();
		this.clusterConfiguration = clusterConfigProvider.getIfUnique();
	}

	protected final RedisStandaloneConfiguration getStandaloneConfiguration() {
		if (this.standaloneConfiguration != null) {
			return this.standaloneConfiguration;
		}
		
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
		config.setHost(properties.getHost());
		config.setPort(properties.getPort());
		config.setPassword(properties.getPassword());
		config.setDatabase(properties.getDatabase());

		return config;
	}

	protected final RedisSentinelConfiguration getSentinelConfiguration() {
		if (this.sentinelConfiguration != null) {
			return this.sentinelConfiguration;
		}

		SentinelConfig setting = properties.getSentinel();
		if (setting == null) return null;

		RedisSentinelConfiguration config = new RedisSentinelConfiguration();
		config.setMasterName(setting.getMasterName());
		config.setSentinelsFromText(setting.getNodes());
		config.setDatabase(properties.getDatabase());
		config.setPassword(properties.getPassword());

		return config;
	}

	protected final RedisClusterConfiguration getClusterConfiguration() {
		if (this.clusterConfiguration != null) {
			return this.clusterConfiguration;
		}

		ClusterConfig setting = properties.getCluster();
		if (setting == null) return null;

		RedisClusterConfiguration config = new RedisClusterConfiguration();
		config.setClusterNodesFromText(setting.getNodes());
		config.setMaxAttempts(setting.getMaxAttempts());
		config.setPassword(properties.getPassword());

		return config;
	}

}
