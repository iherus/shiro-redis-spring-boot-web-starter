/**
 * Copyright (c) 2016-2021, Bosco.Liao (bosco_liao@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.iherus.shiro.redis.spring.boot.autoconfigure;

import java.util.Optional;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.iherus.shiro.cache.redis.config.DefaultPoolConfig;
import org.iherus.shiro.cache.redis.config.RedisClusterConfiguration;
import org.iherus.shiro.cache.redis.config.RedisSentinelConfiguration;
import org.iherus.shiro.cache.redis.config.RedisStandaloneConfiguration;
import org.iherus.shiro.cache.redis.connection.RedisConnectionFactory;
import org.iherus.shiro.cache.redis.connection.lettuce.LettuceConnectionFactory;
import org.iherus.shiro.redis.spring.boot.autoconfigure.ShiroRedisProperties.LettuceConfig;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisClient;

/**
 * 
 * @author Bosco.Liao
 * @since 2.0.0
 */
@Configuration
@ConditionalOnClass({ GenericObjectPool.class, RedisClient.class })
@ConditionalOnMissingClass("org.springframework.data.redis.connection.RedisConnectionFactory")
public class LettuceConnectionFactoryConfiguration extends RedisConfigurationWrapper {

	private final ShiroRedisProperties properties;

	private final ObjectProvider<LettuceClientConfigurationCustomizer> customizerProvider;

	protected LettuceConnectionFactoryConfiguration(ShiroRedisProperties properties,
                                                    ObjectProvider<RedisStandaloneConfiguration> standaloneConfigProvider,
                                                    ObjectProvider<RedisSentinelConfiguration> sentinelConfigProvider,
                                                    ObjectProvider<RedisClusterConfiguration> clusterConfigProvider,
                                                    ObjectProvider<LettuceClientConfigurationCustomizer> customizerProvider) {
		
		super(properties, standaloneConfigProvider, sentinelConfigProvider, clusterConfigProvider);
		this.properties = properties;
		this.customizerProvider = customizerProvider;
	}

	@Bean(name = "x_lettuceConnectionFactory", initMethod = "init", destroyMethod = "destroy")
	@ConditionalOnMissingBean(RedisConnectionFactory.class)
	public LettuceConnectionFactory connectionFactory() {
		return createLettuceConnectionFactory();
	}

	@SuppressWarnings("rawtypes")
	private LettuceConnectionFactory createLettuceConnectionFactory() {
		GenericObjectPoolConfig poolConfig = createLettucePoolConfig(this.properties.getLettuce().getPool());

		RedisSentinelConfiguration sentinelConfig = null;
		RedisClusterConfiguration clusterConfig = null;

		LettuceConnectionFactory factory = null;

		if ((sentinelConfig = getSentinelConfiguration()) != null) {
			factory = new LettuceConnectionFactory(sentinelConfig, poolConfig);
		} else if ((clusterConfig = getClusterConfiguration()) != null) {
			factory = new LettuceConnectionFactory(clusterConfig, poolConfig);
		} else {
			factory = new LettuceConnectionFactory(getStandaloneConfiguration(), poolConfig);
		}

		applyProperties(factory);
		return factory;
	}

	private void applyProperties(LettuceConnectionFactory factory) {
		LettuceConfig lettuceConfig = this.properties.getLettuce();
		LettuceClientConfiguration clientConfig = getLettuceClientConfiguration();

		factory.setClientName(lettuceConfig.getClientName());
		factory.setTimeout(clientConfig.getTimeout());
		factory.setUseSsl(lettuceConfig.isSsl());
		factory.setVerifyPeer(lettuceConfig.isVerifyPeer());
		factory.setStartTls(lettuceConfig.isStartTls());
		factory.setClientResources(clientConfig.getClientResources());
		factory.setClientOptions(clientConfig.getClientOptions());

		Optional.ofNullable(lettuceConfig.getReadFrom()).ifPresent(it -> factory.setReadFrom(ReadFrom.valueOf(it)));
	}

	private LettuceClientConfiguration getLettuceClientConfiguration() {
		LettuceClientConfiguration.Builder builder = LettuceClientConfiguration.builder();
		customize(builder);
		return builder.build();
	}

	@SuppressWarnings("rawtypes")
	private GenericObjectPoolConfig createLettucePoolConfig(ShiroRedisProperties.Pool settingPoolConfig) {
		if (settingPoolConfig == null) {
			return new DefaultPoolConfig();
		}
		DefaultPoolConfig config = new DefaultPoolConfig();
		config.setMinIdle(settingPoolConfig.getMinIdle());
		config.setMaxIdle(settingPoolConfig.getMaxIdle());
		config.setMaxTotal(settingPoolConfig.getMaxTotal());
		config.setMaxWaitMillis(settingPoolConfig.getMaxWaitMillis());
		config.setTestOnBorrow(settingPoolConfig.isTestOnBorrow());
		config.setTestWhileIdle(settingPoolConfig.isTestWhileIdle());
		config.setMinEvictableIdleTimeMillis(settingPoolConfig.getMinEvictableIdleTimeMillis());
		config.setTimeBetweenEvictionRunsMillis(settingPoolConfig.getTimeBetweenEvictionRunsMillis());
		config.setNumTestsPerEvictionRun(settingPoolConfig.getNumTestsPerEvictionRun());

		return config;
	}

	private void customize(LettuceClientConfiguration.Builder builder) {
		this.customizerProvider.orderedStream().forEach((customizer) -> customizer.customize(builder));
	}

}
